package com.drumsampleapp.gainward.drumsampleapp;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ListDataManager {

    private ExecutorService mSingleThreadExecutor;

    private static ListDataManager mInstance;
    private Context mContext;
    private ListDataManager(){
        mSingleThreadExecutor = Executors.newSingleThreadExecutor();
    }

    public static ListDataManager getInstance(){
        if(mInstance == null)mInstance = new ListDataManager();
        return mInstance;
    }

    public void setContext(Context mContext){
        this.mContext = mContext;
    }



    public static class ListDataHolder{
        public String id = null;
        public String name= null;
        public String pathUrl= null;
        public String audioUrl= null;
        public String imageUrl= null;

        public ListDataHolder(){}
        public ListDataHolder(JSONObject object){
            try {
                if(object.has("id"))id = object.getString("id");
                if(object.has("name"))name = object.getString("name");
                if(object.has("path"))pathUrl = object.getString("path");
                if(object.has("audio"))audioUrl = object.getString("audio");
                if(object.has("image"))imageUrl = object.getString("image");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static interface ListDataReadyCallback{
        public abstract void onListDataReady(List<ListDataHolder> dataItemsList );
    }

    public void prepareDataList(final ListDataReadyCallback mCallback){
        mSingleThreadExecutor.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<ListDataHolder> dataList = new ArrayList<>();
                String stringData = readTestDataFromAssetsToString();
                JSONArray array = null;
                try {
                    array = new JSONArray(stringData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(array != null){
                    for(int i=0; i < array.length(); i++){
                        JSONObject object = null;
                        try {
                            object = (JSONObject)array.get(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if(object != null){
                            dataList.add(new ListDataHolder(object));
                        }
                    }

                    mCallback.onListDataReady(dataList);
                    return;
                }

                mCallback.onListDataReady(null);
            }
        });
    }

    private String readTestDataFromAssetsToString(){
        String fileName = "jsondata.txt";
        StringBuilder returnString = new StringBuilder();
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader inputBufferedReader = null;
        try {
            inputStream = mContext.getResources().getAssets() .open(fileName, Context.MODE_WORLD_READABLE);
            inputStreamReader = new InputStreamReader(inputStream);
            inputBufferedReader = new BufferedReader(inputStreamReader);
            String line = "";
            while ((line = inputBufferedReader.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (inputStreamReader != null)inputStreamReader.close();
                if (inputStream != null)inputStream.close();
                if (inputBufferedReader != null)inputBufferedReader.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }

        return returnString.toString();
    }




}
