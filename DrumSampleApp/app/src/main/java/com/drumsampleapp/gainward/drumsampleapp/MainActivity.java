package com.drumsampleapp.gainward.drumsampleapp;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ProgressBar playerProgressBar;
    TextView playerSecondaryLabel;
    View buttonStopPlayer;
    View playerViewsContainer;
    ImageView toolbarImageView;
    private Handler mUiHandler = new Handler();
    SongsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AudioPlayer.getInstance().initWithContext(this);
        setContentView(R.layout.activity_main);
        setUpView();

        getSupportActionBar().hide();

        AudioPlayer.getInstance().setPlayerStatesListener(new AudioPlayer.AudioPlayerStatesLister() {
            @Override
            public void OnPlayerEnded() {
                hidePlayerContainer();
                adapter.notifyItemRangeChanged(0, adapter.mItems.size());
            }

            @Override
            public void OnPlayerReady() {
                showPlayerContainerWhenBufferingEnded();
            }

            @Override
            public void OnPlayerBuffering() {
                showPlayerContainerWhenBuffering();
            }

            @Override
            public void OnPlayerIdle() {
                AudioPlayer.getInstance().stop();
                playerViewsContainer.setVisibility(View.GONE);
                adapter.resetPrevClickPos();
                AudioPlayer.getInstance().setCurrentDataItem(null);
                adapter.notifyItemRangeChanged(0, adapter.mItems.size());
            }

            @Override
            public void OnPlayerError(int errorType) {
                switch (errorType) {
                    case ExoPlaybackException.TYPE_SOURCE:
                        showToastMessage(getString(R.string.eror_404));
                        break;
                    default:
                        showToastMessage(getString(R.string.eror_unknown));
                        break;
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(AudioPlayer.getInstance().getPlayerState() == Player.STATE_READY ){
             AudioPlayer.getInstance().play();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(AudioPlayer.getInstance().isPlaying())AudioPlayer.getInstance().pause();
    }

    private void showPlayerContainerWhenBuffering() {
        playerViewsContainer.setVisibility(View.VISIBLE);
        playerProgressBar.setIndeterminate(true);
    }

    private void showPlayerContainerWhenBufferingEnded() {
        playerViewsContainer.setVisibility(View.VISIBLE);
        playerProgressBar.setIndeterminate(false);
    }

    private void hidePlayerContainer() {
        playerViewsContainer.setVisibility(View.GONE);
    }

    Runnable playerStateRunnable = new Runnable() {
        @Override
        public void run() {
            long duration = AudioPlayer.getInstance().getDurationMsec();
            long position = AudioPlayer.getInstance().getPositionMsec();
            long buffPosition = AudioPlayer.getInstance().getBufferedPositionMsec();

            playerProgressBar.setMax((int) duration);
            playerProgressBar.setProgress((int) position);
            playerProgressBar.setSecondaryProgress((int) buffPosition);

            mUiHandler.postDelayed(playerStateRunnable, 500);
        }
    };

    private void setUpView() {
        playerProgressBar = findViewById(R.id.player_progressbar);
        playerSecondaryLabel = findViewById(R.id.player_secondarylabel);
        buttonStopPlayer = findViewById(R.id.player_stopplayback);
        playerViewsContainer = findViewById(R.id.player_views_container);
        toolbarImageView = findViewById(R.id.toolbar_imageview);

        buttonStopPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AudioPlayer.getInstance().stop();
                playerViewsContainer.setVisibility(View.GONE);
                adapter.resetPrevClickPos();
                AudioPlayer.getInstance().setCurrentDataItem(null);
            }
        });

        ListDataManager manager = ListDataManager.getInstance();
        manager.setContext(this);

        manager.prepareDataList(new ListDataManager.ListDataReadyCallback() {
            @Override
            public void onListDataReady(final List<ListDataManager.ListDataHolder> dataItemsList) {
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (dataItemsList != null) {
                            fillListWithData(dataItemsList);
                        }
                    }
                });
            }
        });
    }

    private void startPlayerProgressStateTimer() {
        stopPlayerProgressStateTimer();
        mUiHandler.postDelayed(playerStateRunnable, 500);
    }

    private void stopPlayerProgressStateTimer() {
        mUiHandler.removeCallbacks(playerStateRunnable);
    }


    private void fillListWithData(final List<ListDataManager.ListDataHolder> dataItemsList) {

        RecyclerView rView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rView.setLayoutManager(layoutManager);

        adapter = new SongsListAdapter();
        adapter.setContext(this);
        adapter.mItems.clear();
        adapter.mItems.addAll(dataItemsList);
        rView.setAdapter(adapter);
        rView.requestFocus();
        rView.smoothScrollToPosition(0);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnPlayButtonClickLister(int prevClickPos, final int position) {

                if (prevClickPos == position) {
                    if (AudioPlayer.getInstance().isPlaying()) {
                        AudioPlayer.getInstance().pause();
                    } else {
                        AudioPlayer.getInstance().play();
                        startPlayerProgressStateTimer();
                    }
                } else {
                    ListDataManager.ListDataHolder dataItem = dataItemsList.get(position);
                    AudioPlayer.getInstance().setCurrentDataItem(dataItem);
                    setUpPlayerLabelAndToolbarImageview(dataItem.name, dataItem.imageUrl);
                    AudioPlayer.getInstance().stop();
                    AudioPlayer.getInstance().prepare(dataItem, dataItem.audioUrl);
                    AudioPlayer.getInstance().play();
                    startPlayerProgressStateTimer();
                    adapter.notifyItemRangeChanged(0, adapter.mItems.size());
                }
            }
        });
    }

    private void setUpPlayerLabelAndToolbarImageview(String songTitle, String imageUrl) {
        playerViewsContainer.setVisibility(View.VISIBLE);
        playerSecondaryLabel.setText(songTitle);

        Glide.with(this)
                .load(imageUrl)
                .centerCrop()
                .into(toolbarImageView);
    }

    public static class ListDataViewHolder extends RecyclerView.ViewHolder {

        ImageView coverImageView;
        TextView titleView;
        Button buttonPlayTrack;
        ProgressBar listItemProgressBar;
        boolean hasConnectedRunnable = false;

        public ListDataViewHolder(View view) {
            super(view);
            coverImageView = view.findViewById(R.id.imageview_cover);
            titleView = view.findViewById(R.id.textview_title);
            buttonPlayTrack = view.findViewById(R.id.button_play_track);
            listItemProgressBar = view.findViewById(R.id.datalistitem_progressbar);
            view.setClickable(true);
        }
    }

    public static class SongsListAdapter extends RecyclerView.Adapter<ListDataViewHolder> {
        public Context mContext;
        private int prevPositionClick = -100;
        public List<ListDataManager.ListDataHolder> mItems = new ArrayList<>();
        private OnItemClickListener onItemClickListener;

        public void setContext(Context __context) {
            mContext = __context;
        }

        @NonNull
        @Override
        public ListDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.songslist_item, null, false);

            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);

            ListDataViewHolder vh = new ListDataViewHolder(itemView);
            return vh;
        }

        Runnable listItemRunnable;
        Handler mUiAdapterHandler = new Handler();

        @Override
        public void onBindViewHolder(@NonNull final ListDataViewHolder holder, final int position) {

            ListDataManager.ListDataHolder listItem = mItems.get(position);

            if (listItem.imageUrl != null) {
                Glide.with(mContext)
                        .load(listItem.imageUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .centerCrop()
                        .into(holder.coverImageView);
            }
            holder.titleView.setText(listItem.name);
            if (listItem.audioUrl == null) holder.buttonPlayTrack.setVisibility(View.GONE);
            holder.buttonPlayTrack.setText(mContext.getString(R.string.start_playback));
            holder.buttonPlayTrack.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (onItemClickListener != null) {
                                onItemClickListener.OnPlayButtonClickLister(prevPositionClick, position);
                            }
                            prevPositionClick = position;

                            if(AudioPlayer.getInstance().isPlaying()){
                                holder.buttonPlayTrack.setText(mContext.getString(R.string.pause_playback));
                            }else{
                                holder.buttonPlayTrack.setText(mContext.getString(R.string.start_playback));
                            }
                        }
                    });

            if (AudioPlayer.getInstance().getCurrentDataItem() == null) {
                holder.listItemProgressBar.setVisibility(View.GONE);
                holder.hasConnectedRunnable = false;
                return;
            }

            if (mItems.indexOf(AudioPlayer.getInstance().getCurrentDataItem()) == position) {
                if(AudioPlayer.getInstance().isPlaying()){
                    holder.buttonPlayTrack.setText(mContext.getString(R.string.pause_playback));
                }else{
                    holder.buttonPlayTrack.setText(mContext.getString(R.string.start_playback));
                }
                if (listItemRunnable != null) mUiAdapterHandler.removeCallbacks(listItemRunnable);
                holder.listItemProgressBar.setVisibility(View.VISIBLE);
                holder.hasConnectedRunnable = true;

                listItemRunnable = new Runnable() {
                    @Override
                    public void run() {
                        if (AudioPlayer.getInstance().getCurrentDataItem() == null) {
                            holder.listItemProgressBar.setVisibility(View.GONE);
                            holder.hasConnectedRunnable = false;
                            return;
                        }
                        long duration = AudioPlayer.getInstance().getDurationMsec();
                        long position = AudioPlayer.getInstance().getPositionMsec();
                        long buffPosition = AudioPlayer.getInstance().getBufferedPositionMsec();
                        switch (AudioPlayer.getInstance().getPlayerState()) {
                            case Player.STATE_READY:
                                holder.listItemProgressBar.setIndeterminate(false);
                                holder.listItemProgressBar.setMax((int) duration);
                                holder.listItemProgressBar.setProgress((int) position);
                                holder.listItemProgressBar.setSecondaryProgress((int) buffPosition);
                                break;
                            case Player.STATE_BUFFERING:
                                holder.listItemProgressBar.setIndeterminate(true);
                                break;
                            default:
                                holder.listItemProgressBar.setIndeterminate(false);
                                holder.listItemProgressBar.setMax((int) duration);
                                holder.listItemProgressBar.setProgress((int) position);
                                holder.listItemProgressBar.setSecondaryProgress((int) buffPosition);
                                break;
                        }
                        mUiAdapterHandler.postDelayed(listItemRunnable, 500);
                    }
                };
                mUiAdapterHandler.postDelayed(listItemRunnable, 500);
            } else {
                holder.listItemProgressBar.setVisibility(View.GONE);
                holder.hasConnectedRunnable = false;
            }
        }

        @Override
        public void onViewAttachedToWindow(@NonNull ListDataViewHolder holder) {
            super.onViewAttachedToWindow(holder);

            if (holder.hasConnectedRunnable) {
                holder.listItemProgressBar.setVisibility(View.VISIBLE);
                if (listItemRunnable != null) mUiAdapterHandler.postDelayed(listItemRunnable, 500);
            } else {
                holder.listItemProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onViewDetachedFromWindow(@NonNull ListDataViewHolder holder) {
            super.onViewDetachedFromWindow(holder);

            if (holder.hasConnectedRunnable) {
//                mUiAdapterHandler.removeCallbacks(listItemRunnable);
            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void setOnItemClickListener(OnItemClickListener __onItemClickListener) {
            onItemClickListener = __onItemClickListener;
        }

        public void resetPrevClickPos() {
            prevPositionClick = -100;
        }
    }

    public static interface OnItemClickListener {

        public abstract void OnPlayButtonClickLister(int prevPositionClick, int position);

    }

    private void showToastMessage(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}
