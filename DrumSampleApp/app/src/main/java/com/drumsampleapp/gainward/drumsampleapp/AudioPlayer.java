package com.drumsampleapp.gainward.drumsampleapp;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

public class AudioPlayer {
    private AudioPlayerStatesLister mStatesListener;
    private static AudioPlayer mInstance;
    private SimpleExoPlayer player;
    private Context mContext;
    private AudioPlayer(){}
    private boolean isReady;
    private ListDataManager.ListDataHolder dataItem;

    public static AudioPlayer getInstance(){
        if(mInstance == null)mInstance = new AudioPlayer();
        return mInstance;
    }

    public void initWithContext(Context __mContext){
        mContext = __mContext;
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(__mContext, trackSelector);
    }

    public void setCurrentDataItem(ListDataManager.ListDataHolder __dataItem){
        dataItem = __dataItem;
    }

    public ListDataManager.ListDataHolder getCurrentDataItem(){
        return dataItem;
    }

    public int getPlayerState(){
        return player.getPlaybackState();
    }

    public void prepare( ListDataManager.ListDataHolder ____dataItem, final String url  ){
        dataItem = ____dataItem;
        isReady = false;
        try {
            player.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch(playbackState){
                        case Player.STATE_ENDED:
                            setCurrentDataItem(null);
                            mStatesListener.OnPlayerEnded();
                            break;
                        case Player.STATE_READY:
                            isReady = true;
                            mStatesListener.OnPlayerReady();
                            break;
                        case Player.STATE_BUFFERING:
                            isReady = false;
                            mStatesListener.OnPlayerBuffering();
                            break;
                        case Player.STATE_IDLE:
                            setCurrentDataItem(null);
                            mStatesListener.OnPlayerIdle();
                            break;
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    mStatesListener.OnPlayerError( error.type );
                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                    seek(0);
                    play();
                }

                @Override
                public void onSeekProcessed() {

                }
            });
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mContext, Util.getUserAgent(mContext, mContext.getString(R.string.app_name)), bandwidthMeter);
            MediaSource audioSource = new ExtractorMediaSource
                    .Factory(dataSourceFactory)
                    .createMediaSource( Uri.parse( url ));
            player.prepare(audioSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play(){
        player.setPlayWhenReady(true);
    }

    public void pause(){
        player.setPlayWhenReady(false);
    }

    public void seek(int msec){
        player.seekTo(msec);
    }

    public void stop(){
        isReady = false;

        player.stop();
    }

    public void release(){
        isReady = false;

        player.stop();
        player.release();
//        player = null;

//        mContext = null;
    }

    public void setVolume(float volume){
        player.setVolume( volume );
    }

    public boolean isPaused(){
        if(player == null)return false;
        return player.getPlayWhenReady();
    }

    public boolean isPlaying(){
        if(player == null)return false;
        return player.getPlayWhenReady();
    }
    public long getPositionMsec(){
        return isReady ? player.getCurrentPosition() : 0;
    }
    public long getDurationMsec(){
        return isReady ? player.getDuration() : 0;
    }

    public long getBufferedPositionMsec(){
        return isReady ? player.getBufferedPosition() : 0;
    }

//                            case Player.STATE_ENDED:
////                            if(mListener != null)mListener.onPlaybackEnd();
//            break;
//                        case Player.STATE_READY:
//    isReady = true;
//                            break;
//                        case Player.STATE_BUFFERING:
//    isReady = false;
//                            break;

    public static interface AudioPlayerStatesLister{
        public abstract void OnPlayerEnded();
        public abstract void OnPlayerReady();
        public abstract void OnPlayerBuffering();
        public abstract void OnPlayerIdle();
        public abstract void OnPlayerError(int type);
    }

    public void setPlayerStatesListener(AudioPlayerStatesLister __mStatesListener){
        mStatesListener = __mStatesListener;
    }

}
